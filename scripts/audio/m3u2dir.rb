config = "m3u2dir".get_config rescue {}

[
  -> () { # config & arguments normalization
    options = parse_args do |parser, opts|
      parser.on("-p", "--playlist PLAYLIST",
                "source playlist (m3u format)") do |playlist|
        opts[:playlist] = playlist
      end

      parser.on("-d", "--directory directory",
                "destination directory (if not existing, it will be " +
                "automatically created)") do |directory|
        opts[:directory] = directory
      end
    end

    config[:src_playlist] = options[:playlist].to_pn
    config[:out_dir] = options[:directory].to_pn

    true
  },
  -> () { # validation -> source playlist
    if !config[:src_playlist].file?
      "invalid source playlist #{config[:src_playlist]}: not a file.".perr
    else
      "selecting source playlist #{config[:src_playlist]}.".pinf
    end
  },
  -> () { # validation -> output directory
    if !config[:out_dir].directory?
      if config[:out_dir].exist?
        "invalid output directory #{config[:out_dir]}: not a directory.".perr
      else
        "creating output directory #{config[:out_dir]}.".pinf
        config[:out_dir].mkpath
        true
      end
    else
      "selecting output directory #{config[:out_dir]}.".pinf
    end
  },
  -> () { # validation -> source files
    config[:src_files] = config[:src_playlist].readlines.
        reject { |line| line.start_with?("#") }.
        map { |line| line.to_pn }
  },
  -> () { # check external requirements
    true
  },
  -> () {
    config[:src_files].each_with_index do |src_file, index|
      index += 1
      prefix = sprintf("%03d", index)

      out_name = "#{prefix} - #{src_file.basename}"
      out_file = config[:out_dir].join(out_name)

      "cp".run(src_file, out_file)
    end
  }
].do_all auto_exit_code: true
