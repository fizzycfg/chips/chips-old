config = "telegram-multi".get_config rescue {}

[
  -> () { # Check external requirements
    "telegram-desktop".check_program!
  },
  -> () { # Config normalization
    config[:profiles] = {} unless config[:profiles]

    true
  },
  -> () { # Parse arguments
    options = parse_args do |parser, opts|
      parser.on("--only x,y,z", Array,
                "Launch only specific profiles " +
                "(available: `#{config[:profiles].keys}`)") do |profiles|
        opts[:enabled_profiles] = profiles
      end
    end

    config[:enabled_profiles] = options[:enabled_profiles] || config[:profiles].keys
    config[:enabled_profiles] = config[:enabled_profiles].map { |p| p.to_s.to_sym }

    true
  },
  -> () { # Validate profiles data types
    config[:profiles].all? do |name, data|
      if data.is_a?(Hash)
        true
      else
        "Invalid data type for profile #{name.as_tok}".perr
      end
    end
  },
  -> () { # Validate enabled profiles are defined
    invalid_profiles = config[:enabled_profiles].reject do |enabled_profile|
      config[:profiles].keys.include?(enabled_profile)
    end

    if invalid_profiles.length > 0
      "Undefined profiles: #{invalid_profiles.as_tok}".perr
    else
      "Selected profiles: #{config[:enabled_profiles]}".pinf
    end
  },
  -> () { # Add fields to profiles
    config[:profiles].select do |name, _|
      config[:enabled_profiles].include?(name)
    end.each do |name, data|
      unless config[:workdir]
        data[:workdir] = "~/.local/share/TelegramDesktop/#{name}"
      end
      data[:workdir] = data[:workdir].to_pn.expand_path
    end

    true
  },
  -> () { # Create workdirs
    config[:profiles].each do |name, data|
      data[:workdir].mkpath unless data[:workdir].directory?
    end

    true
  },
  -> () {
    config[:profiles].all? do |name, data|
      "telegram-desktop".run("-workdir", data[:workdir], detached: true)
    end
  }
].do_all auto_exit_code: true
